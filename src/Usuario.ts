class Usuario {
    id: number;
    name: string;
    username: string;
    email: string;
    address: {
        street: string,
        suite: string,
        city: string,
        zipcode: string,
        geo: {
            lat: string,
            lng: string
        }
    };
    phone: string;
    website: string;
    company: {
        name: string,
        catchPhrase: string,
        bs: string,
    }

    constructor(usuario: any) {
        this.id = usuario.id;
        this.name = usuario.name;
        this.username = usuario.username;
        this.email = usuario.email;
        this.address = usuario.address;
        this.phone = usuario.phone;
        this.website = usuario.website;
        this.company = usuario.company;
    }

    public getUser(): any {
        return '<br><br>' +
            'id: ' + this.id + '<br>' +
            'name: ' + this.name + '<br>' +
            'username: ' + this.username + '<br>' +
            'email: ' + this.email + '<br>' +
            'address: <br>' +
            'street: ' + this.address.street + '<br>' +
            'suite: ' + this.address.suite + '<br>' +
            'city: ' + this.address.city + '<br>' +
            'zipcode: ' + this.address.zipcode + '<br>' +
            'geo: <br>' +
            'lat: ' + this.address.geo.lat + '<br>' +
            'lng: ' + this.address.geo.lng + '<br>' +
            'phone: ' + this.phone + '<br>' +
            'website: ' + this.website + '<br>' +
            'company: <br>' +
            'name: ' + this.company.name + '<br>' +
            'catchPhrase: ' + this.company.catchPhrase + '<br>' +
            'bs: ' + this.company.bs
    }
}

export default Usuario;