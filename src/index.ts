import express from 'express';
import server from './server';

const PORT = process.env.PORT || 3000;

server.server.listen(PORT, () => {
    console.log('Running at http://localhost:3000');
});