import express from "express";
import request from "request";
import Usuario from "./Usuario";

const server = express();

let user;
let nomesOrdem = [];
let suites = [];

function getNomesOrdem(nome: string) {
    let aux = nome.split('-/-');
    nomesOrdem.push('<br><br>Name: ' + aux[0] + '<br>Email: ' + aux[1] + '<br>Company: ' + aux[2]);
};

function getAddressWithSuite(address: string) {
    if (address.includes('Suite ')) {
        suites.push(user.getUser());
    }
};

server.get("/", (_, res) => {
    request('https://jsonplaceholder.typicode.com/users', function (error, response, body) {

        let resp = JSON.parse(body);
        // let user;

        let websites = [];
        let nomes = [];
        // let nomesOrdem = [];
        // let suites = [];

        resp.forEach(x => {
            user = new Usuario(x);
            websites.push('<br> - ' + user.website);
            nomes.push(user.name + '-/-' + user.email + '-/-' + user.company.name);

            getAddressWithSuite(user.address.street + user.address.suite);
        });

        nomes = nomes.sort();
        nomes.forEach(nome => {
            getNomesOrdem(nome);
        });

        res.send('websites:' + websites + '<hr>' + nomesOrdem + '<hr><br>' + suites);
    });

});

// export default server;
export = { server, getNomesOrdem, getAddressWithSuite }